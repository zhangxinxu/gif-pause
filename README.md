# 让GIF可以暂停播放

### 介绍
可以让GIF图片点击后暂停播放，基于 ImageDecoder 实现的。

#### 演示demo

访问这里：https://zhangxinxu.gitee.io/gif-pause/

### 使用说明

1.  引入对应的 JS 文件，例如：
```html
<script src="./src/renderGif.js"></script>
```
2.  对应的GIF图像元素进行调用：
```js
renderGif(eleImage);
```

### 语法和参数

#### 语法
```js
const player = renderGif(data, options);
```
#### 参数

**data**

```data``` 参数可缺省，如果不设置，表示获取当前页面中所有以 '.gif' 为后缀的 IMG 元素并进行处理。

```data``` 参数可以是字符串，表示对应元素选择器；可以是DOM对象；也可以是 NodeList 对象，或者 HTMLCollection、HTMLAllCollection 对象。

**options**

```options``` 是可选参数，目前仅支持一个属性值。

```js
{
    bindEvent: true
}
```
表示是否给GIF图片绑定点击暂停行为，如果设置 bindEvent 为 false，可以使用返回值 ```player``` 对GIF的播放和暂停进行手动控制。

#### 返回值

```player``` 返回值包含了以下属性和方法：

```js
{   
    // 当前 GIF 图像元素对象
    element: null,
    // GIF 是否暂停中，只读
    paused: false,
    // 继续播放
    play: function () {},
    // 暂停播放
    pause: function () {},
    // 当前帧，只读
    frameIndex: -1
}
```

#### 其他

ImageDecoder 这个 API 目前仅 Chrome 浏览器支持，如果希望任意现代浏览器都支持 GIF 点击暂停，可以试试 libgif.js 这个项目。

更多信息，例如实现原理，可以死参见这篇文章：<a href="https://www.zhangxinxu.com/wordpress/?p=10830">https://www.zhangxinxu.com/wordpress/?p=10830</a>
